<?php include('./includes/header.php');  ?> 


 
<div class="row">
  
<div class="col-xs-12">   

<div class="bt_sections nf"> 
  


<h1>Politicas de Privacidad</h1> 

<hr>



      <p> El siguiente Aviso de Privacidad establece la forma en la que MULTIBRAND OUTLET STORES, S.A.P.I. DE C.V. en adelante PROMODA, recolectará, utilizará, almacenará, eliminará o tratará sus datos personales. Se entiende por “Datos Personales” cualquier información relativa a una persona física identificada o identificable. PROMODA como responsable de los datos que usted proporciona, brindara la protección y tratamiento de los datos personales que el titular, que es la persona física a quien le corresponden los datos personales, ponga a disposición de PROMODA </p>
      <h3>Las finalidades del tratamiento de los datos personales que el titular pone a disposición de PROMODA son:</h3>
      <ol class="fq_points">
         <li>
            <p>
               Proveer servicios y productos requeridos;
            </p>
         </li>
         <li>
            <p>
               Puedes cambiar tu compra por cualquier otro producto del sitio o en los locales, de la misma colección&nbsp;ó puedes escoger por obtener una eCard por el importe de tu compra.
            </p>
         </li>
         <li>
            <p>
               Informar sobre nuevos productos o servicios que estén relacionados con el contratado o adquirido por el cliente;
            </p>
         </li>
         <li>
            <p>
               Dar cumplimiento a obligaciones contraídas con nuestros clientes;
            </p>
            <p>
               Informar sobre cambios de nuestros productos o servicios;
            </p>
            <p>
               Evaluar la calidad del servicio, y
            </p>
         </li>
         <li>
            <p>
               Realizar estudios internos sobre hábitos de consumo
            </p>
         </li>
      </ol>
      <h3>Sus datos personales pueden ser transferidos de conformidad con el artículo 37 de la ley en los siguientes casos:</h3>
      <ol class="fq_points">
         <li>
            <p>
               Proveer servicios y productos requeridos;
            </p>
         </li>
         <li>
            <p>
               Puedes cambiar tu compra por cualquier otro producto del sitio o en los locales, de la misma colección&nbsp;ó puedes escoger por obtener una eCard por el importe de tu compra.
            </p>
         </li>
         <li>
            <p>
               Informar sobre nuevos productos o servicios que estén relacionados con el contratado o adquirido por el cliente;
            </p>
         </li>
         <li>
            <p>
               Dar cumplimiento a obligaciones contraídas con nuestros clientes;
            </p>
            <p>
               Informar sobre cambios de nuestros productos o servicios;
            </p>
            <p>
               Evaluar la calidad del servicio, y
            </p>
         </li>
         <li>
            <p>
               Realizar estudios internos sobre hábitos de consumo
            </p>
         </li>
      </ol>












   
</div>

</div>

</div>






<?php include('./includes/footer.php');  ?> 