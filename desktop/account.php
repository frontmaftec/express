<?php include('./includes/header.php');  ?> 

</div>



<div class="fondo_cuenta"> 

<div class="container">

<div class="row">
  
<div class="col-xs-12"><h1 class="my_account">Mi cuenta</h1></div>

</div>



<div class="row">
	
<div class="sidebar"> 

<div class="col-xs-3 sr">
	
<ul class="dotted_line"> 
   
   <li class="selected">  <a href="#"><span class="mis_pedidos icon"></span> <span>Mis pedidos</span>  </a> </li>
   <li>  <a href="#"><span class="mis_devoluciones icon"></span> <span>Mis devoluciones</span></a>      </li>
   <li>  <a href="#"><span class="mis_cupones icon"></span> <span> Mis cupones </span> </a>  </li>
   <li>  <a href="#"><span class="mi_informacion icon"></span> <span> Información de cuenta </span> </a>  </li>
   <li>  <a href="#"><span class="mis_direcciones icon"></span> <span> Libreta de direcciones </span> </a>  </li>
   <li>  <a href="#"><span class="mis_tarjetas icon"></span> <span> Mis Tarjetas </span> </a>  </li>
   <li>  <a href="#"><span class="mis_favoritos icon"></span> <span> Favoritos </span> </a>  </li>
   <li>  <a href="#"><span class="mis_noti icon"></span> <span> Notificaciones via Email? </span> </a>  </li>

</ul>  
 
</div>

</div>


<div class="col-xs-9 sl">






<div class="user_panel">
    
  <h2>MIS PEDIDOS</h2>
  <p class="t_standar"> Usted no ha realizado Pedidos </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-3"> FECHA </div>
      <div class="col-xs-3"> PEDIDO # </div>
      <div class="col-xs-3"> DIRECCIÓN </div>
      <div class="col-xs-3 text-right"> TOTAL PRICE </div>
   </div>
 


   <!-- Producto Agregado --> 

   <div class="row rl"> 

      <div class="col-xs-3 data_general f_auto"> 
         <h4>12/03/2016</h4>
      </div>

      <div class="col-xs-3 data_general f_auto">
          <h4>399S03S9VV</h4>
      </div>

      <div class="col-xs-3 f_auto"> 
         <p class="normal_18">Calle Altitud #222</p> 
         <p>Colonia Polanco</p>
      </div>

      

      <div class="col-xs-3 data_general f_auto">
         <h4 class="text-right">$98.00</h4>
      </div>

   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>











<div class="user_panel">
    
  <h2>MIS DEVOLUCIONES</h2>
  <p class="t_standar"> Usted no ha realizado Devoluciones </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-2"> PEDIDO # </div>
      <div class="col-xs-2"> SKU </div>
      <div class="col-xs-3"> FECHA DE DEVOLUCIÓN </div>
      <div class="col-xs-3"> MOTIVO </div>
      <div class="col-xs-2 text-right"> CODIGO RASTREO </div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl"> 

      <div class="col-xs-2 data_general f_auto"> 
         <h4> 740 </h4>
      </div> 

      <div class="col-xs-2 data_general f_auto">
          <h4>XYZN3S9202</h4>
      </div>

      <div class="col-xs-3 data_general f_auto">
          <h4>15/10/2016</h4>
      </div>

      <div class="col-xs-3 f_auto">
         <p class="normal_18"> La talla no era la adecuada</p>
      </div>

      

      <div class="col-xs-2 data_general f_auto">
         <h4 class="text-right">732839282812392</h4>
      </div>

   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>














<div class="user_panel">
    
  <h2>MIS CUPONES</h2>
  <p class="t_standar"> Usted no tiene cupones </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-6"> CLAVE CUPON # </div>
      <div class="col-xs-6"> FECHA DE EXPIRACIÓN </div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl"> 

      <div class="col-xs-6 data_general f_auto"> 
         <h4> JULIO02DESCUENTO </h4>
      </div> 

      <div class="col-xs-6 data_general f_auto">
          <h4>31/07/2016</h4>
      </div>


   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>









 




<div class="user_panel">
    

  <h2>INFORMACIÓN DE LA CUENTA</h2>


<div class="row">

     <div class="col-xs-12"> 
  
  <ul class="panel_info">
   <li> <p>SEXO: <span> Masculino  </span> </p></li>     
   <li> <p>E-MAIL: <span> ernesto@pictures.com  </span> </p></li>     
   <li> <p>NOMBRE: <span> Ernesto Cárdenas  </span> </p></li>   
   <li> <p>FECHA DE NACIMIENTO: <span> 15/10/1990  </span> </p></li> 
  </ul>

     </div>

 </div>


</div>










<div class="user_panel">
    
  <h2>INFORMACIÓN DE LA CUENTA</h2>

<div class="row">

<form role="form" action="#" class="form_edit">

            <div class="form-group"> 
               <input type="text" class="form-control w10 " id="#" placeholder="Email">
            </div>
 

            <div class="form-group">
               <select class="gender">
                  <option value="">Sexo</option>
                  <option value="male">Masculino</option>
                  <option value="female">Femenino</option>
               </select>
            </div>

 

            <div class="form-group">
               <input type="text" class="form-control w10" id="#" placeholder="Nombre">
            </div>
            <div class="form-group">
               <input type="text" class="form-control w10" id="#" placeholder="Apellido">
            </div>

   

            <div class="form-group">
 
<div class="col-xs-4 pf">
               <select class="date w10">
<option value="" selected="selected">DD</option>
<option value="01">1</option>
<option value="02">2</option>
<option value="03">3</option>
<option value="04">4</option>
<option value="05">5</option>
<option value="06">6</option>
<option value="07">7</option>
<option value="08">8</option>
<option value="09">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
               </select>
</div>

<div class="col-xs-4 fp"> 
               <select class="date w10">
<option value="" selected="selected">MM</option>
<option value="01">ENE</option>
<option value="02">FEB</option>
<option value="03">MAR</option>
<option value="04">ABR</option>
<option value="05">MAY</option>
<option value="06">JUN</option>
<option value="07">JUL</option>
<option value="08">AGO</option>
<option value="09">SEP</option>
<option value="10">OCT</option>
<option value="11">NOV</option>
<option value="12">DIC</option>
               </select>
</div>

<div class="col-xs-4 fp"> 
               <select class="date w10"> 
<option value="" selected="selected">YYYY</option>
<option value="1916">1916</option>
<option value="1917">1917</option>
<option value="1918">1918</option>
<option value="1919">1919</option>
<option value="1920">1920</option>
<option value="1921">1921</option>
<option value="1922">1922</option>
<option value="1923">1923</option>
<option value="1924">1924</option>
<option value="1925">1925</option>
<option value="1926">1926</option>
<option value="1927">1927</option>
<option value="1928">1928</option>
               </select>
</div>
 

            </div>


<p>* Campos Obligatorios</p>

            <button type="submit" class="btn btn-express register btn-default">GUARDAR</button> 
         </form>



</div>


</div>















<div class="user_panel">
  
  <h2> Editar Dirección </h2>

<div class="row">  


<form role="form" action="#" class="form_comun">



            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="Nombre">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="Apellido">
</div>

            </div>




            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="Teléfono">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="Teléfono Adicional">
</div>

            </div>



            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="Código Postal">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="Calle">
</div>

            </div>



            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="No. Exterior">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="No. Interior">
</div>

            </div>




            <div class="form-group">
               <select class="gender">
                  <option value="">Colonia *</option>
                  <option value="male">ESTRELLA</option>
                  <option value="female">1</option>
               </select>
            </div>





            <div class="form-group">
 
<div class="col-xs-6 pf">
               <select class="date w10">
<option value="" selected="selected">Estado *</option>
<option value="01">1</option>
<option value="02">2</option>
<option value="03">3</option>
<option value="04">4</option>
<option value="05">5</option>
<option value="06">6</option>
<option value="07">7</option>
<option value="08">8</option>
               </select>
</div>

<div class="col-xs-6 fp"> 
               <select class="date w10">
<option value="" selected="selected">Ciudad *</option>
<option value="01">ENE</option>
<option value="02">FEB</option>
<option value="03">MAR</option>
<option value="04">ABR</option>

               </select>
</div>



            </div>


<p>* Campos Obligatorios</p>

            <button type="submit" class="btn btn-express register btn-default">GUARDAR</button> 
         </form>


</div>

</div>









<div class="user_panel">
    
  <h2>MIS TARJETAS</h2>
  <p class="t_standar"> Usted no tiene tarjetas </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-4"> TARJETA </div>
      <div class="col-xs-4"> TERMINACIÓN </div>
      <div class="col-xs-4"> COMPRAS </div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl"> 

      <div class="col-xs-4 data_general f_auto"> 
         <h4> VISA </h4>
      </div> 

      <div class="col-xs-4 data_general f_auto">
          <h4>... .... 232</h4>
      </div>


      <div class="col-xs-4 data_general f_auto">
          <h4>05</h4>
      </div>


   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>














<div class="user_panel">
    
  <h2>FAVORITOS</h2>
  <p class="t_standar"> Usted no tiene Favoritos </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-2"> PRODUCTO </div>
      <div class="col-xs-2"> FECHA </div>
      <div class="col-xs-3"> DISPONIBILIDAD </div>
      <div class="col-xs-3"> PRECIO </div>
      <div class="col-xs-2 text-right">  </div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl"> 

      <div class="col-xs-2 data_general f_auto"> 
         <img class="img-responsive" src="./images/catalog/1.jpg" alt=""> 
      </div> 

      <div class="col-xs-2 data_general f_auto">
          <h4>15/10/2016</h4>
      </div>

      <div class="col-xs-3 data_general f_auto">
          <h4>Out of stock</h4>
      </div>

      <div class="col-xs-3 f_auto">
         <p class="normal_18"> 923 MXN</p>
      </div>

      

      <div class="col-xs-2 data_general f_auto">
         <h4 class="text-right">Sin notas</h4>
      </div>

   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>





















<div class="user_panel">
    
  <h2>ADMINISTRAR NEWSLETTER</h2> 


<div class="row"> 

     <div class="col-xs-12"> 
     
<div class="select_news">

                <form action="#">  

                  <input style="margin-right: 5px;" id="nb1" value="nb1" name="nb1" type="checkbox">
                  <label for="nb1" class="#">Newsletter</label>
                <br> 
                  <input style="margin-right: 5px;" id="nb2" value="nb2" name="nb2" type="checkbox">
                  <label for="nb2" class="#">Notificaciones De  Marketing</label>
              
              <hr>

 <button type="submit" class="btn btn-express btn-default">ENVÍAR</button>

                </form>

</div>

     </div>

 </div>

</div>














</div> <!-- col 9 --> 




</div>

</div>


</div>



<?php include('./includes/footer.php');  ?>  